package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener{
float deposit1, rate1, year1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.calculate);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.setting);
		b2.setOnClickListener(this);
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int id = v.getId();
		if (id == R.id.calculate) {
			EditText deposit = (EditText)findViewById(R.id.deposit);
		deposit1 = Float.parseFloat(deposit.getText().toString());
		
		TextView rate = (TextView)findViewById(R.id.rate);
		rate1 = Float.parseFloat(rate.getText().toString());
		
		EditText year = (EditText)findViewById(R.id.year);
		year1 = Float.parseFloat(year.getText().toString());
		
		TextView total = (TextView)findViewById(R.id.total);
		//float calrate1 = rate1/100;
		
			String res = "";
			try {
				
				double total1 = deposit1 * (Math.pow(1+(rate1/100),year1));
				res = String.format(Locale.getDefault(), "%.2f", total1);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			total.setText(res);
		}
		else if (id == R.id.setting) {
			Intent i = new Intent(this, SettingActivity.class);
			//rate1 = rate1*100;
			i.putExtra("rate1", rate1);
			startActivityForResult(i, 8888);
			// 8888 is a request code. It is a user-defined unique integer.
			// It is used to identify the return value
		}
		
	}
	
	
	// receiving returned value
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (requestCode == 8888 && resultCode == RESULT_OK) {
				rate1 = data.getFloatExtra("rate1", 32.0f);
				TextView rate = (TextView)findViewById(R.id.rate);
				rate.setText(String.format(Locale.getDefault(), "%.2f", rate1));
			}
		}

}
